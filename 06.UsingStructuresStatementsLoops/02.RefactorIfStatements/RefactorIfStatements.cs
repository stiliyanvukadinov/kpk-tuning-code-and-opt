﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.RefactorIfStatements
{
    public class RefactorIfStatements
    {
        static void Main()
        {
            Potato potato = new Potato();
            //... 
            if (potato != null && potato.HasBeenPeeled && !potato.IsRotten)
            {
                Cook(potato);
            }

            int x = 0;
            int y = 0;
            const int MinX = -200;
            const int MaxX = 200;
            const int MinY = -200;
            const int MaxY = 200;

            bool shouldVisitCell = true;

            if ((MinX <= x && x <= MaxX) && (MinY <= y && y <= MaxY) && shouldVisitCell)
            {
               VisitCell();
            }

        }

        private static void Cook(Potato potato)
        {
            throw new NotImplementedException();
        }

        private static void VisitCell()
        {
            throw new NotImplementedException();
        }
    }
}
