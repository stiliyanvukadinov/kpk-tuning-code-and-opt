﻿using System;

public class ConsolePrinter
{
    private const int MaxCount = 6;

    public class Methods
    {
        public void PrintBoolValue (bool boolVariable)
        {
            string variableAsString = boolVariable.ToString();
            Console.WriteLine(variableAsString);
        }
    }
    public static void Main()
    {
        ConsolePrinter.Methods cp = new ConsolePrinter.Methods();
        cp.PrintBoolValue(true);
    }
}
