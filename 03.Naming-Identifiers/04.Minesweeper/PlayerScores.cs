﻿using System;

namespace Minesweeper
{
    public class PlayerScores
    {
        private string name;
        private int scores;

        public PlayerScores()
        {
        }

        public PlayerScores(string name, int scores)
        {
            this.Name = name;
            this.Scores = scores;
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public int Scores
        {
            get { return this.scores; }
            set { this.scores = value; }
        }
    }
}
