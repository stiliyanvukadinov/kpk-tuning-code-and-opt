﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Methods;

namespace Methods
{
    public class Test_Methods
    {
        static void Main()
        {
            Console.WriteLine(Methods.CalcTriangleArea(3, 4, 5));

            Console.WriteLine(Methods.TranslateDigit(5));

            Console.WriteLine(Methods.FindMax(5, -1, 3, 2, 14, 2, 3));

            Methods.PrintNumber(1.3, 2);
            Methods.PrintPersent(0.75, 2);
            Methods.PrintAligned(2.30, 8);

            Console.WriteLine(Methods.CalcDistance(3, -1, 3, 2.5));
            Console.WriteLine("Horizontal? " + Methods.IsLineHorizontal(3, 3));
            Console.WriteLine("Vertical? " + Methods.IsLineVertical(-1, 2.5));

            Student peter = new Student() { FirstName = "Peter", LastName = "Ivanov" };
            peter.OtherInfo = "From Sofia, born at 17.03.1992";

            Student stella = new Student() { FirstName = "Stella", LastName = "Markova" };
            stella.OtherInfo = "From Vidin, gamer, high results, born at 03.11.1993";

            Console.WriteLine("{0} older than {1} -> {2}",
                peter.FirstName, stella.FirstName, peter.IsOlderThan(stella));
        }
    }
}
