﻿using System;

namespace Methods
{
    public class Methods
    {
        /// <summary>
        /// Calculates Area of a triangle using Herone formula
        /// </summary>
        /// <param name="a">Fist side of triangle</param>
        /// <param name="b">Second side of triangle</param>
        /// <param name="c">Third side of triangle</param>
        /// <returns>Returns the triangle area</returns>
        public static double CalcTriangleArea(double a, double b, double c)
        {
            if (a <= 0 || b <= 0 || c <= 0)
            {
                throw new ArgumentOutOfRangeException("Sides of triangle must be positive numbers.");
            }
            if (a + b < c || b + c < a || c + a < b)
            {
                throw new ArgumentOutOfRangeException("a,b and c don't make triangle.");
            }

            // Calculate semiperimeter s
            double s = (a + b + c) / 2;
            double area = Math.Sqrt(s * (s - a) * (s - b) * (s - c));
            return area;
        }

        /// <summary>
        /// Translate digit in English
        /// </summary>
        /// <param name="number">Insert digit.</param>
        /// <returns>Returns digit as string.</returns>
        public static string TranslateDigit(int number)
        {
            switch (number)
            {
                case 0: return "zero";
                case 1: return "one";
                case 2: return "two";
                case 3: return "three";
                case 4: return "four";
                case 5: return "five";
                case 6: return "six";
                case 7: return "seven";
                case 8: return "eight";
                case 9: return "nine";
                default: throw new ArgumentOutOfRangeException("You must enter a digit - value in the range [0...9].");
            }
        }

        /// <summary>
        /// Find max number from array
        /// </summary>
        /// <param name="elements">Array of int numbers.</param>
        /// <returns>Returns max number.</returns>
        public static int FindMax(params int[] elements)
        {
            if (elements == null || elements.Length == 0)
            {
                throw new NullReferenceException("Array must have almost one element.");
            }
            int maxElement = elements[0];
            for (int i = 1; i < elements.Length; i++)
            {
                if (elements[i] > maxElement)
                {
                    maxElement = elements[i];
                }
            }
            return maxElement;
        }

        /// <summary>
        /// Print to the console decimal number.
        /// </summary>
        /// <param name="number">Inserts a number</param>
        /// <param name="decimals">Inserts a number after decimal seperator</param>
        public static void PrintNumber(object number, int decimals)
        {
            string format = "{0:f" + decimals + "}";
            Console.WriteLine(format, number);
        }

        /// <summary>
        /// Print to the console number as persent.
        /// </summary>
        /// <param name="number">Inserts a number</param>
        /// <param name="decimals">Inserts a number after decimal seperator</param>
        public static void PrintPersent(object number, int decimals)
        {
           string format = "{0:p" + decimals + "}";
           Console.WriteLine(format, number);
        }

        /// <summary>
        /// Print to the console aligned number.
        /// </summary>
        /// <param name="number">Inserts a number.</param>
        /// <param name="decimals">Inserts lenght of output string.</param>
        public static void PrintAligned(object number, int length)
        {
            string format = "{0," + length + "}";
            Console.WriteLine("{0,8}", number);
        }        

        /// <summary>
        /// Calculate distatnce between two points.
        /// </summary>
        /// <param name="x1">X coordinate of first point</param>
        /// <param name="y1">Y coordinate of first point</param>
        /// <param name="x2">X coordinate of second point</param>
        /// <param name="y2">Y coordinate of second point</param>
        /// <returns></returns>
        public static double CalcDistance(double x1, double y1, double x2, double y2)
        {            
            double xProjection = (x2 - x1);
            double yProjection = (y2 - y1);
            double distance = Math.Sqrt(xProjection * xProjection + yProjection * yProjection);
            return distance;
        }

        /// <summary>
        /// Check if line is horizontal.
        /// </summary>
        /// <param name="x1">X coordinate of first point.</param>
        /// <param name="x2">X coordinate of second point.</param>
        /// <returns>Returns whether line is horizontal.</returns>
        public static bool IsLineHorizontal(double x1, double x2)
        {
            if (x1 == x2)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check if line is vertical.
        /// </summary>
        /// <param name="y1">Y coordinate of first point.</param>
        /// <param name="y2">Y coordinate of second point.</param>
        /// <returns>Returns whether line is vertical.</returns>
        public static bool IsLineVertical(double y1, double y2)
        {
            if (y1 == y2)
            {
                return true;
            }
            return false;
        }
        
    }
}
